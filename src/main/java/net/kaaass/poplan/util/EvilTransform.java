package net.kaaass.poplan.util;

import com.mapdigit.gis.geometry.GeoLatLng;

/**
 * 坐标系转换 From:http://blog.csdn.net/liuzhoulong/article/details/36198453
 * 
 * @author qifengdao, KAAAsS
 */
public class EvilTransform {

	final static double pi = 3.14159265358979324;
	// a = 6378245.0, 1/f = 298.3
	// b = a * (1 - f)
	// ee = (a^2 - b^2) / a^2;
	final static double a = 6378245.0;
	final static double ee = 0.00669342162296594323;

	/**
	 * 地理坐标系坐标转火星坐标系坐标
	 * 
	 * @param geoPos
	 *            地理坐标系坐标
	 * @return 火星坐标系坐标
	 */
	public static GeoLatLng transToMars(GeoLatLng geoPos) {
		double[] result = transform(geoPos.lat(), geoPos.lng());
		return new GeoLatLng(result[0], result[1]);
	}

	/**
	 * 火星坐标系坐标转地理坐标系坐标(估值，偏移较小地区精度高)
	 * 
	 * @param marPos
	 *            火星坐标系坐标
	 * @return 地理坐标系坐标(约值)
	 */
	public static GeoLatLng transToGeo(GeoLatLng marPos) {
		double[] result = transform(marPos.lat(), marPos.lng());
		return new GeoLatLng(2 * marPos.lat() - result[0], 2 * marPos.lng()
				- result[1]);
	}

	// World Geodetic System ==> Mars Geodetic System
	private static double[] transform(double wgLat, double wgLon) {
		double mgLat = 0;
		double mgLon = 0;
		if (outOfChina(wgLat, wgLon)) {
			mgLat = wgLat;
			mgLon = wgLon;

		} else {
			double dLat = transformLat(wgLon - 105.0, wgLat - 35.0);
			double dLon = transformLon(wgLon - 105.0, wgLat - 35.0);
			double radLat = wgLat / 180.0 * pi;
			double magic = Math.sin(radLat);
			magic = 1 - ee * magic * magic;
			double sqrtMagic = Math.sqrt(magic);
			dLat = (dLat * 180.0) / ((a * (1 - ee)) / (magic * sqrtMagic) * pi);
			dLon = (dLon * 180.0) / (a / sqrtMagic * Math.cos(radLat) * pi);
			mgLat = wgLat + dLat;
			mgLon = wgLon + dLon;
		}
		double[] point = { mgLat, mgLon };
		return point;
	}

	private static boolean outOfChina(double lat, double lon) {
		if (lon < 72.004 || lon > 137.8347)
			return true;
		if (lat < 0.8293 || lat > 55.8271)
			return true;
		return false;
	}

	private static double transformLat(double x, double y) {
		double ret = -100.0 + 2.0 * x + 3.0 * y + 0.2 * y * y + 0.1 * x * y
				+ 0.2 * Math.sqrt(Math.abs(x));
		ret += (20.0 * Math.sin(6.0 * x * pi) + 20.0 * Math.sin(2.0 * x * pi)) * 2.0 / 3.0;
		ret += (20.0 * Math.sin(y * pi) + 40.0 * Math.sin(y / 3.0 * pi)) * 2.0 / 3.0;
		ret += (160.0 * Math.sin(y / 12.0 * pi) + 320 * Math.sin(y * pi / 30.0)) * 2.0 / 3.0;
		return ret;
	}

	private static double transformLon(double x, double y) {
		double ret = 300.0 + x + 2.0 * y + 0.1 * x * x + 0.1 * x * y + 0.1
				* Math.sqrt(Math.abs(x));
		ret += (20.0 * Math.sin(6.0 * x * pi) + 20.0 * Math.sin(2.0 * x * pi)) * 2.0 / 3.0;
		ret += (20.0 * Math.sin(x * pi) + 40.0 * Math.sin(x / 3.0 * pi)) * 2.0 / 3.0;
		ret += (150.0 * Math.sin(x / 12.0 * pi) + 300.0 * Math.sin(x / 30.0
				* pi)) * 2.0 / 3.0;
		return ret;
	}
}
