package net.kaaass.poplan.util;

import com.mashape.unirest.request.body.RequestBodyEntity;

public class StringUtil {
    public static void checkHeader(RequestBodyEntity req) {
        req.getHttpRequest().getHeaders().forEach((k, v) -> {
            System.out.println(k + ": " + v.get(0));
        });
    }
}
