package net.kaaass.poplan.ingress.api;

import net.kaaass.poplan.util.StringUtil;
import org.json.JSONObject;

public class Ingress {
    public static Auth auth = null;

    public static void cookieLogin(String cookie) {
        auth = new Auth(cookie);
    }

    public static void login(String usr, String passwd) {
        auth = new Auth(usr, passwd);
    }

    public static JSONObject requestAPI(String url, JSONObject param) throws Exception {
        if (auth == null) throw new NullPointerException("Please first login!");
        StringUtil.checkHeader(auth.authPost(IngressAPIs.API_BASE + url, param));
        String response = auth.authPost(IngressAPIs.API_BASE + url, param).asString().getBody();
        System.out.println(response);
        return new JSONObject(response);
    }
}
