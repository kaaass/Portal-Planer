package net.kaaass.poplan.ingress.entities;

public class CField {
	public static  final int HARVEST_AP = 1250;

	/**
	 * 形成cf的poA
	 */
	public Portal poA;
	/**
	 * 形成cf的poB
	 */
	public Portal poB;
	/**
	 * 形成cf的poC
	 */
	public Portal poC;

	public CField(Portal poA, Portal poB, Portal poC) {
		this.poA = poA;
		this.poB = poB;
		this.poC = poC;
	}
}
