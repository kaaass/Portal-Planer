package net.kaaass.poplan.ingress.entities;

import com.mapdigit.gis.geometry.GeoLatLng;

public class Link {
	/**
	 * 完成Link所获得的AP
	 */
	public static final int HARVEST_AP = 313;

	/**
	 * 连出link的po
	 */
	public Portal poA;
	/**
	 * 被link的po
	 */
	public Portal poB;

	public Link(Portal poA, Portal poB) {
		this.poA = poA;
		this.poB = poB;
	}

	/**
	 * 连出link的po的位置
	 * @return 位置
	 */
	public GeoLatLng getPosA() {
		return this.poA.getLocation();
	}

	/**
	 * 被link的po的位置
	 * @return 位置
	 */
	public GeoLatLng getPosB() {
		return this.poB.getLocation();
	}
}
