package net.kaaass.poplan.ingress;

public class TooMuchLinksException extends Exception {

	private static final long serialVersionUID = 1L;

	public TooMuchLinksException(String string) {
		super(string);
	}
}
