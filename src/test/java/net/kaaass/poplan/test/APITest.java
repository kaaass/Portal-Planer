package net.kaaass.poplan.test;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.mashape.unirest.request.body.RequestBodyEntity;
import net.kaaass.poplan.ingress.api.Entities;
import net.kaaass.poplan.ingress.api.Ingress;
import net.kaaass.poplan.util.LocationUtil;
import net.kaaass.poplan.util.StringUtil;
import org.junit.Before;
import org.junit.Test;

public class APITest {
    String cookie = "csrftoken=cEBUpRo4RChqOwrWwWg58VeROGIJkARR; SACSID=~AJKiYcEXdJqrMrj-6bA64oDQ0Cwo_sMlAbWoIb1F4FwBVlFMhGjvSLsVJRQVoHg8AaYIsgyn2W4nPGmfoTvaXNsm3YLXhepYrQZ42SW3fbQY4R51jQi_vr0H9YSjKvsaA9ErxEPg2CL0RGYesvwfN3QsghIGH5-4F_hbwJVuvxtVQtdCGTB_Yqb1M4RMC7HCvUtQr98L9Q5eTtb4IWpYznt7zUMTR-gqcs6W8F_pPc00G_jxLaNb04dba2kCHzthHlmWt3QkxwVgmNesDyfIT48aZsxDBwXfILJL4ACsqoXR9kLlmzkZzN4HJ189nknXtdoBb-6uPSnh;";

    @Before
    public void auth() {
        System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
        Ingress.cookieLogin(cookie);
    }

    @Test
    public void testVersion() throws Exception {
        /*
        try {
            RequestBodyEntity req = Unirest.post("https://ingress.com/r/getEntities").header("X-CSRFToken", "cEBUpRo4RChqOwrWwWg58VeROGIJkARR")
                    .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2816.0 Safari/537.36")
                    .header("Referer", "https://ingress.com/intel")
                    .header("Cookie", this.cookie).body("{\"tileKeys\":[\"12_6681_3351_0_8_100\"],\"v\":\"e2ee3c71b29cd4ff4c9d5ec8ef1d50af2653aa6d\"}");
            HttpResponse<String> res = req.asString();
            StringUtil.checkHeader(req);
            System.out.println(res.getBody());
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        */
        // ====
        //System.out.println(LocationUtil.toTileKeys(120.65, 28.01, 15, 0, 8));
        System.out.println(Entities.fetchEntitie("12_6681_3351_0_8_100"));
        // Entities.getPortals(Entities.fetchEntities(120.5641365051, 28.0426628606, 120.8126378059, 27.9054941358, 15));
    }
}
