package net.kaaass.poplan.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import net.kaaass.poplan.ingress.entities.Portal;
import net.kaaass.poplan.ingress.entities.Portal.ResonatorItem;

import org.junit.Test;

import com.mapdigit.gis.geometry.GeoLatLng;

public class PortalTest {

	@Test
	public void testPortal() {
		List<ResonatorItem> reses = new ArrayList<ResonatorItem>();
		reses.add(new ResonatorItem(3));
		reses.add(new ResonatorItem(2));
		reses.add(new ResonatorItem(5));
		reses.add(new ResonatorItem(3));
		reses.add(new ResonatorItem(4));
		reses.add(new ResonatorItem(2));
		reses.add(new ResonatorItem(3));
		reses.add(new ResonatorItem(3));
		Portal wz = new Portal(new GeoLatLng(27.993926D, 120.698532D),
				Portal.RESISTANCE, reses, null);
		assertEquals(wz.getCamp(), Portal.RESISTANCE);
		assertEquals(wz.getLevel(), 3);
		assertEquals(wz.getLinkLimit(), 12960);
	}

}
