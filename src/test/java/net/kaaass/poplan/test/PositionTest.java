package net.kaaass.poplan.test;

import static org.junit.Assert.*;
import net.kaaass.poplan.util.EvilTransform;

import org.junit.Test;

import com.mapdigit.gis.geometry.GeoLatLng;
import com.mapdigit.gis.location.ChinaMapOffset;

public class PositionTest {

	@Test
	public void testEvilTransform() {
		GeoLatLng wzGeo = new GeoLatLng(27.993926D, 120.698532D); // 温州大剧院地理坐标
		ChinaMapOffset cmo = new ChinaMapOffset();
		System.out.println(cmo.fromEarthToMars(wzGeo));
		assertEquals(EvilTransform.transToMars(wzGeo), new GeoLatLng(
				27.99064253901607, 120.70258876773497)); // Test EvilTransform
															// with map
	}

}
